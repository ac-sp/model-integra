<?php

namespace Model\Integra;

class Usuario {
    
    use \Model\Integra\WS_Integra;
    
    function cadastraUsuario($codIntegra,$nome,$usuario) {
        $client = new SoapClient($this->wsdl, array("trace" => 1));

        $parameters = array("Usuario" => array(
                "Consumidor" => array(
                    "Chave" => $this->user,
                    "Senha" => $this->password
                ),
                "Usuario" => array(
                    "CodigoAssociado" => $codIntegra,
                    "NomeCompleto" => $nome,
                    "NomeUsuario" => $usuario,
                    "TrocaSenha" => "N"
                )
            )
        );
        $result = $client->CadastraUsuario($parameters);
        // \Acsp\Log::get()->add("Cadastro Usuarios - parametros :");
        // \Acsp\Log::get()->add(var_export($parameters, true));
        // \Acsp\Log::get()->add("cadastro usuario xml enviado :");
        // \Acsp\Log::get()->add($client->__getLastRequest());
        // \Acsp\Log::get()->add("retorno :");
        // \Acsp\Log::get()->add(var_export($result, true));
        return $result->CadastraUsuarioResult;
    }

    function alteraUsuario($codIntegra,
            $codUsuario,
            $nome,
            $status) {
        $client = new SoapClient($this->wsdl, array("trace" => 1));

        $parameters = array("Usuario" => array(
                "Consumidor" => array(
                    "Chave" => $this->user,
                    "Senha" => $this->password
                ),
                "Usuario" => array(
                    "CodigoAssociado" => $codIntegra,
                    "CodigoUsuario" => $codUsuario,
                    "NomeCompleto" => $nome,
                    "Status" => $status,
                    "Senha" => '',
                    "ConfirmaSenha" => '',
                )
            )
        );

        $result = $client->AlteraUsuario($parameters);
        // \Acsp\Log::get()->add("Altera Usuario - parametros :");
        // \Acsp\Log::get()->add(var_export($parameters, true));
        // \Acsp\Log::get()->add("alteracao usuario xml enviado :");
        // \Acsp\Log::get()->add($client->__getLastRequest());
        // \Acsp\Log::get()->add("retorno :");
        // \Acsp\Log::get()->add(var_export($result, true));

        return $result->AlteraUsuarioResult;
    }

    function consultaUsuario($codIntegra) {
        $client = new SoapClient($this->wsdl, array("trace" => 1));
        $parameters = array("Usuario" => array(
                "Consumidor" => array(
                    "Chave" => $this->user,
                    "Senha" => $this->password
                ),
                "Usuario" => array(
                    "CodigoAssociado" => $codIntegra,
                    "CodigoUsuario" => '',
                    "NomeCompleto" => '',
                    "NomeUsuario" => ''
                )
            )
        );
        $result = $client->ConsultaUsuario($parameters);
        return $result->ConsultaUsuarioResult;
    }   

}